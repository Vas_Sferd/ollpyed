from kivy.uix.screenmanager import Screen
from kivymd.uix.button import MDFlatButton


class MainUi(Screen):
    def __init__(self):
        super().__init__()
        button = MDFlatButton(
            text="Hello worlds",
            pos_hint={"center_x": 0.8, "center_y": 0.5}
        )
        button2 = MDFlatButton(
            text="Hello world",
            pos_hint={"center_x": 0.2, "center_y": 0.5}
        )

        self.add_widget(button2)
        self.add_widget(button)
