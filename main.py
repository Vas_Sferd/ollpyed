from kaki.app import App
from kivymd.app import MDApp

from kivy.factory import Factory
from kivy.core.window import Window


Window.size = (500, 500)


class OllPyEdApp(App, MDApp):
    CLASSES = {
        "MainUi": "widgets.main_ui"
    }

    AUTORELOADER_PATHS = [
        (".", {"recursive": True})
    ]

    def build_app(self, *args):
        return Factory.MainUi()


def run():
    OllPyEdApp().run()


if __name__ == '__main__':
    run()
