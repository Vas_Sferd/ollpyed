import os
import sys
import subprocess


def run():
    try:
        env = os.environ.copy()
        env["DEBUG"] = "1"
        subprocess.check_call(f"{sys.executable} main.py", env=env, shell=True)
    except Exception as e:
        print("Script are fallen!\n")
        raise e
